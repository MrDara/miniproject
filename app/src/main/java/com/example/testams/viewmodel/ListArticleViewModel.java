package com.example.testams.viewmodel;

import android.net.Uri;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.testams.model.api.entities.Article;
import com.example.testams.model.api.entities.Pagination;
import com.example.testams.model.api.entities.PostArticle;
import com.example.testams.model.api.responses.DeleteResponse;
import com.example.testams.model.api.responses.ServerResponseImage;
import com.example.testams.model.api.repository.ArticleRepository;
import com.example.testams.model.api.responses.ListArticleResponse;
import com.example.testams.model.api.responses.UpdateResponse;
import com.example.testams.view.main.MainActivity;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class ListArticleViewModel extends ViewModel {

    private MutableLiveData<ListArticleResponse> liveData;
    private MutableLiveData<ServerResponseImage> uploadImage;
    private MutableLiveData<PostArticle> postArticle;
    private MutableLiveData<Article> getArticleById;
    private MutableLiveData<DeleteResponse> deleteArticle;
    private MutableLiveData<UpdateResponse> updateResponseMutableLiveData;

    private ArticleRepository articleRepository;
    Pagination pagination;
    File file;
    int id;
    PostArticle postArticles;

    RequestBody fileBody;
    MultipartBody.Part image;

    public void init(){
        articleRepository = new ArticleRepository();
        pagination = new Pagination();
        liveData = articleRepository.getListArticleByPagination(pagination);
    }

    // TODO: implement setter and getter

    public void setPostArticle(PostArticle postArticle) {
        this.postArticles = postArticle;
    }


    public MutableLiveData<ListArticleResponse> getLiveData(){
        return liveData;
    }


    public void setFile(File file) {
        this.file = file;
    }

    public MutableLiveData<PostArticle> getPostArticle() {
        return postArticle;
    }

    public MutableLiveData<ServerResponseImage> getUploadImage(){
        return  uploadImage;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public MutableLiveData<Article> getGetArticleById() {
        return getArticleById;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MutableLiveData<DeleteResponse> getDeleteArticle() {
        return deleteArticle;
    }

    public MutableLiveData<UpdateResponse> getUpdateResponseMutableLiveData() {
        return updateResponseMutableLiveData;
    }

    public void setFileBody(RequestBody fileBody) {
        this.fileBody = fileBody;
    }

    public void setImage(MultipartBody.Part image) {
        this.image = image;
    }

    // TODO: implement update insert delete in article

    public void intiPostArticle(){
        postArticle = articleRepository.postArticle(postArticles);
    }

    public void initUploadImage(){
        uploadImage = articleRepository.getUrlImage(fileBody,image);
    }

    public void FetchListArticle(){
        Log.d("PAGE","Page"+pagination.getPage());
        liveData = articleRepository.getListArticleByPagination(pagination);
    }

    public void initGetArticleById(){
        getArticleById = articleRepository.getArticleById(id);
    }

    public void initDeleteArticle(){
        deleteArticle = articleRepository.DeleteArticle(id);
    }

    public void initUpdateArticle(){
        updateResponseMutableLiveData = articleRepository.updateArticle(id, postArticles);
    }

}
