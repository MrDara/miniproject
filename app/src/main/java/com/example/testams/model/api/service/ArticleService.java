package com.example.testams.model.api.service;

import com.example.testams.model.api.entities.Article;
import com.example.testams.model.api.entities.PostArticle;
import com.example.testams.model.api.responses.DeleteResponse;
import com.example.testams.model.api.responses.ServerResponseImage;
import com.example.testams.model.api.responses.ListArticleResponse;
import com.example.testams.model.api.responses.UpdateResponse;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("articles")
    Call<ListArticleResponse> getAll(@Query("page") long page,@Query("limit") long limit);

    @POST("articles")
    Call<PostArticle>  insertArticle(@Body PostArticle postArticle);

    @Multipart
    @POST("uploadfile/single")
    Call<ServerResponseImage> updateProfile(@Part("file") RequestBody file,@Part MultipartBody.Part image);

    @DELETE("articles/{id}")
    Call<DeleteResponse> deleteArticle(@Path("id")int id);

    @GET("articles/{id}")
    Call<Article> getById(@Path("id") int id);

    @PUT("articles/{id}")
    Call<UpdateResponse> updateArticle(@Path("id")int id,@Body PostArticle postArticle);



}
