package com.example.testams.model.api.config;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static final String API_KEY="Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=";
    private static OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(UrlConstant.BASE_URL_AMS);

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass){

        okHttpBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request= chain.request();
                Request.Builder reqBuilder = request.newBuilder()
                        .addHeader("Authorization",API_KEY)
                        .header("Accept","application/json")
                        .method(request.method(),request.body());
                return chain.proceed(reqBuilder.build());
            }

        });
        return builder
                .client(okHttpBuilder.build())
                .build().create(serviceClass);
    }
}
