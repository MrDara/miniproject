package com.example.testams.model.api.repository;


import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.testams.model.api.config.ServiceGenerator;
import com.example.testams.model.api.entities.Article;
import com.example.testams.model.api.entities.Pagination;
import com.example.testams.model.api.entities.PostArticle;
import com.example.testams.model.api.responses.DeleteResponse;
import com.example.testams.model.api.responses.ServerResponseImage;
import com.example.testams.model.api.responses.ListArticleResponse;
import com.example.testams.model.api.responses.UpdateResponse;
import com.example.testams.model.api.service.ArticleService;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private ArticleService service;

    public ArticleRepository(){
        this.service = ServiceGenerator.createService(ArticleService.class);
    }

    //TODO: detail all about method below
    /*
    * All method use to connect Server from Repository for observe data change.
    * Call is class of Retrofit use for Call back result.
    * MutableLiveData<> use for modify or send object for Observe.
    * */

    // This method use for get Article by Pagination.
    public MutableLiveData<ListArticleResponse> getListArticleByPagination(Pagination pagination){

        // use for store observe.
        final MutableLiveData<ListArticleResponse> liveData = new MutableLiveData<>();

        // TODO : implement retrofit
        /*
        * use service from ArticleService interface
        * use call for store data that get from api
        *
        * */
        Call<ListArticleResponse> call = service.getAll(pagination.getPage(),pagination.getLimit());

        call.enqueue(new Callback<ListArticleResponse>() {
            @Override
            public void onResponse(Call<ListArticleResponse> call, Response<ListArticleResponse> response) {
                if (response.isSuccessful()){
                    Log.d("ArticleRepository","ArticleRepository : "+ response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListArticleResponse> call, Throwable t) {
                Log.d("ArticleRepository","ArticleRepositoryError : "+t.getMessage());
            }
        });

        return liveData;

    }

    public MutableLiveData<ServerResponseImage> getUrlImage(RequestBody file,MultipartBody.Part image){

        final MutableLiveData<ServerResponseImage> liveDataImage = new MutableLiveData<>();

        Call<ServerResponseImage> uploadImage = service.updateProfile(file, image);
        Log.d("Process","Repository Upload image");
        uploadImage.enqueue(new Callback<ServerResponseImage>() {
            @Override
            public void onResponse(Call<ServerResponseImage> call, Response<ServerResponseImage> response) {
                if (response.isSuccessful()){
                    Log.d("Upload","UploadImage: "+ response.body().getMessage());
                    liveDataImage.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ServerResponseImage> call, Throwable t) {
                Log.d("Upload","UploadImage Error : "+ t.getMessage());
            }
        });

        return liveDataImage;
    }


    public MutableLiveData<PostArticle> postArticle(PostArticle postArticle){

        final MutableLiveData<PostArticle>  liveDataPost = new MutableLiveData<>();

        Call<PostArticle> postArticles = service.insertArticle(postArticle);
        Log.d("Post","PostArticle" + postArticle);
        postArticles.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("Post","Success");
                liveDataPost.setValue((PostArticle) response.body());
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Post","UploadImage: "+ t.getMessage());
            }
        });

        return liveDataPost;
    }

    public MutableLiveData<Article> getArticleById(int id){

        // use it for Observe data change connect by object below.
        final MutableLiveData<Article>  liveDataGetArticle = new MutableLiveData<>();

        // Call to connect Retrofit.
        Call<Article> getArticle = service.getById(id);
        getArticle.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Call<Article> call, Response<Article> response) {
                Log.d("Post","Get Article Success");
                liveDataGetArticle.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Article> call, Throwable t) {

            }
        });

        return liveDataGetArticle;
    }

    public MutableLiveData<DeleteResponse> DeleteArticle(int id){

        final MutableLiveData<DeleteResponse>  liveDataDeleteArticle = new MutableLiveData<>();

        Call<DeleteResponse> call = service.deleteArticle(id);

        call.enqueue(new Callback<DeleteResponse>() {
            @Override
            public void onResponse(Call<DeleteResponse> call, Response<DeleteResponse> response) {
                Log.d("Delete","Delete Article Success");
                liveDataDeleteArticle.setValue(response.body());
            }

            @Override
            public void onFailure(Call<DeleteResponse> call, Throwable t) {
                Log.d("Delete","Delete Article hasn't Success"+t.getMessage());
            }
        });

        return liveDataDeleteArticle;
    }

    public MutableLiveData<UpdateResponse> updateArticle(int id,PostArticle postArticle){
        final MutableLiveData<UpdateResponse> update = new MutableLiveData<>();

        Call<UpdateResponse> updateResponseCall = service.updateArticle(id, postArticle);

        updateResponseCall.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                Log.d("Update","Update Article Success");
                update.setValue(response.body());
            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                Log.d("Update","Update Article hasn't Success"+t.getMessage());
            }
        });

        return update;
    }

}
