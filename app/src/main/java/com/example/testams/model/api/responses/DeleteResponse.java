package com.example.testams.model.api.responses;

import com.example.testams.model.api.entities.Article;
import com.example.testams.model.api.entities.Pagination;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeleteResponse {

    @SerializedName("pagination")
    private Pagination pagination;
    @SerializedName("data")
    private Article data;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    public DeleteResponse() {
    }

    public DeleteResponse(Pagination pagination, Article data, String message, int code) {
        this.pagination = pagination;
        this.data = data;
        this.message = message;
        this.code = code;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public Article getData() {
        return data;
    }

    public void setData(Article data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
