package com.example.testams.model.api.responses;

import com.example.testams.model.api.entities.Article;
import com.example.testams.model.api.entities.Pagination;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListArticleResponse {

    @SerializedName("pagination")
    private Pagination pagination;
    @SerializedName("data")
    private List<Article> data;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<Article> getData() {
        return data;
    }

    public void setData(List<Article> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ListArticleResponse{" +
                "pagination=" + pagination +
                ", data=" + data +
                ", message='" + message + '\'' +
                ", code=" + code +
                '}';
    }
}
