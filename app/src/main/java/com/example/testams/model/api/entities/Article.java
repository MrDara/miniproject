package com.example.testams.model.api.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Article implements Serializable {
    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("title")
    private String title;
    @SerializedName("id")
    private int id;
    @SerializedName("description")
    private String description;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Article{" +
                "imageUrl='" + imageUrl + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", title='" + title + '\'' +
                ", id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
