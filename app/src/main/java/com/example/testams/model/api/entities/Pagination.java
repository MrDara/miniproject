package com.example.testams.model.api.entities;

import com.google.gson.annotations.SerializedName;

public class Pagination {
    @SerializedName("total_pages")
    private int totalPages;
    @SerializedName("total_count")
    private int totalCount;
    @SerializedName("limit")
    private int limit;
    @SerializedName("page")
    private int page;

    public Pagination() {
        this.page = 1;
        this.limit = 15;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
