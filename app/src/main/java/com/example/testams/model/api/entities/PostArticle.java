package com.example.testams.model.api.entities;

import com.google.gson.annotations.SerializedName;

public class PostArticle {

    @SerializedName("category_id")
    private int categoryId;
    @SerializedName("author_id")
    private int authorId;
    @SerializedName("image")
    private String image;
    @SerializedName("status")
    private String status;
    @SerializedName("description")
    private String description;
    @SerializedName("title")
    private String title;

    public PostArticle() {
    }

    public PostArticle(int categoryId, int authorId, String image, String status, String description, String title) {
        this.categoryId = categoryId;
        this.authorId = authorId;
        this.image = image;
        this.status = status;
        this.description = description;
        this.title = title;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
