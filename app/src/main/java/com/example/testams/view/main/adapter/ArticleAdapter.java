package com.example.testams.view.main.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.bumptech.glide.Glide;
import com.example.testams.R;
import com.example.testams.model.api.entities.Article;

import java.util.List;

public class ArticleAdapter extends  Adapter<ArticleAdapter.MyViewHolder>{

    private List<Article> dateSet;
    Context context;

    public interface GetItem{
        void getItemByClick(Article article);
        void getItemFormDelete(Article article);
        void getItemFormUpdate(Article article);
    }

    GetItem getItem;


    public ArticleAdapter(Context context, List<Article> dateSet,GetItem getItem) {
        this.dateSet = dateSet;
        this.context = context;
        this.getItem = getItem;
    }

    public void setDateSet(List<Article> dateSet){
        this.dateSet = dateSet;
    }

    public List<Article> getDataSet(){
        return dateSet;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.title.setText(dateSet.get(position).getTitle());
        try {
            if(dateSet.get(position).getImageUrl() == null){
                Glide.with(context)
                        .load(R.drawable.ramadan)
                        .placeholder(R.drawable.ramadan)
                        .into(holder.imageView);
            }else{
                Glide.with(context)
                        .load(dateSet.get(position).getImageUrl())
                        .placeholder(R.drawable.ramadan)
                        .into(holder.imageView);
            }
        }catch (Exception e){
            Log.d("Error","No Image");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getItem.getItemByClick(dateSet.get(position));
            }
        });

        holder.option_digit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(context,holder.option_digit);
                popupMenu.inflate(R.menu.recycle_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()){
                            case R.id.edit:
                                getItem.getItemFormUpdate(dateSet.get(position));
                                break;
                            case R.id.remove:
                                getItem.getItemFormDelete(dateSet.get(position));
                                notifyDataSetChanged();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return dateSet.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView imageView,option_digit;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_title);
            imageView = itemView.findViewById(R.id.image_view_item);
            option_digit = itemView.findViewById(R.id.optionDigit);
        }
    }

    public void add(Article  article){
        dateSet.add(article);
        notifyItemInserted(dateSet.size()-1);
    }

    public void addAll(List<Article> articles){
        for(Article article: articles){
            add(article);
        }
    }

    public void addBottomItem(){
        add(new Article());
    }

    public void removedLastEmptyItem(){
        int position = dateSet.size()-1;
        Article article = getArticleItem(position);

        if (article!=null){
            dateSet.remove(position);
            notifyItemChanged(position);
        }

    }

    private Article getArticleItem(int position) {

        return dateSet.get(position);

    }

}
