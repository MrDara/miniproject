package com.example.testams.view.helper;

import com.example.testams.model.api.entities.Article;

public interface MyButtonClickListener {
    void onClick(int pos);
}
