package com.example.testams.view.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.testams.R;
import com.example.testams.model.api.entities.Article;
import com.example.testams.model.api.responses.DeleteResponse;
import com.example.testams.model.api.responses.ListArticleResponse;
import com.example.testams.view.add.AddActivity;
import com.example.testams.view.detail.DetailActivity;
import com.example.testams.view.helper.MyButtonClickListener;
import com.example.testams.view.helper.MySwipeHelper;
import com.example.testams.view.helper.Pagination;
import com.example.testams.view.main.adapter.ArticleAdapter;
import com.example.testams.view.update.UpdateActivity;
import com.example.testams.viewmodel.ListArticleViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;


public class MainActivity extends AppCompatActivity implements ArticleAdapter.GetItem {

    private ListArticleResponse dataSet;
    private ListArticleViewModel listArticleViewModel;

    private RecyclerView recyclerView;

    private ArticleAdapter adapter;

    private SwipeRefreshLayout swContainer;
    private FloatingActionButton button;

    LinearLayoutManager layoutManager;
    CoordinatorLayout coordinatorLayout;
    public int currentPage = 1;

    List<Article> articleList;

    ProgressBar progressBar;

    private static int START_PAGE = 1;
    private int TOTAL_PAGE = 15;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int CURRENT_PAGE = START_PAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

            initLayout();
            connectViewModel();

            getLiveDataFromViewModel();
            setUpSwipeRefreshLayout();

            MySwipeHelper swipeHelper = new MySwipeHelper(this,recyclerView,300) {
                @Override
                public void instantiateMyButton(final RecyclerView.ViewHolder viewHolder, List<MySwipeHelper.MyButton> buffer) {
                    buffer.add(new MyButton(MainActivity.this,
                                    "Delete",
                                    50,
                                    R.drawable.ic_baseline_delete_24,
                                    Color.parseColor("#D627D9"),
                                    new MyButtonClickListener() {
                                        @Override
                                        public void onClick(int pos) {
                                            Toast.makeText(MainActivity.this,"Postition : "+ pos,Toast.LENGTH_LONG).show();
//                                            DeleteArticle(pos);
                                        }
                                    }
                            )
                    );
                    buffer.add(new MyButton(MainActivity.this,
                                    "Update",
                                    30,
                                    R.drawable.ic_baseline_edit_24,
                                    Color.parseColor("#FF9502"),
                                    new MyButtonClickListener() {
                                        @Override
                                        public void onClick(int pos) {
                                            Toast.makeText(MainActivity.this,"Postition : "+ pos,Toast.LENGTH_LONG).show();
//                                            UpdateArticle(pos);
                                        }
                                    }
                            )
                    );
                }
            };


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.top:
                Toast.makeText(this,"TOP" ,Toast.LENGTH_LONG).show();

                recyclerView.scrollToPosition(0);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // TODO : Connect View to ViewModel
    private void connectViewModel(){
        listArticleViewModel = ViewModelProviders.of(this).get(ListArticleViewModel.class);
        listArticleViewModel.init();
    }

    // TODO : use fo initialize Layout in MainActivity.
    private void initLayout(){
        recyclerView = findViewById(R.id.recyclerview);
        swContainer = findViewById(R.id.swipe_container);
        button = findViewById(R.id.home_button_add);
        coordinatorLayout = findViewById(R.id.container_coordinator);
        layoutManager = new LinearLayoutManager(this);

        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.INVISIBLE);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });

    }

    // TODO : setup RecyclerView in Activity.
    private void setUpRecyclerView(){
        if(adapter == null){
            adapter = new ArticleAdapter(MainActivity.this,dataSet.getData(), this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapter);
            articleList = adapter.getDataSet();
        }else{
            Log.d("SetData","New Data");
            adapter.setDateSet(dataSet.getData());
            adapter.notifyDataSetChanged();
        }

        // TODO: implement recyclerView pagination
        recyclerView.addOnScrollListener(new Pagination(layoutManager) {
            @Override
            protected void loadMoreItem() {

                isLoading = true;
                CURRENT_PAGE +=1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);

            }

            @Override
            protected int getTotalPages() {
                return TOTAL_PAGE;
            }

            @Override
            protected boolean isLastPage() {
                return isLastPage;
            }

            @Override
            protected boolean isLoading() {
                return isLoading;
            }
        });


    }

    private void loadNextPage() {
        listArticleViewModel.getPagination().setPage(CURRENT_PAGE);
        listArticleViewModel.FetchListArticle();
        listArticleViewModel.getLiveData().observe(this, new Observer<ListArticleResponse>() {
            @Override
            public void onChanged(ListArticleResponse listArticleResponse) {
                dataSet = listArticleResponse;
                adapter.removedLastEmptyItem();
                isLoading = false;
                adapter.addAll(dataSet.getData());
                if (CURRENT_PAGE<=TOTAL_PAGE){
                    adapter.addBottomItem();
                }else{
                    isLastPage = true;
                }
            }
        });
    }

    // TODO : method run by Observer run.
    private void getLiveDataFromViewModel(){
        listArticleViewModel.getLiveData().observe(this, new Observer<ListArticleResponse>() {
            @Override
            public void onChanged(ListArticleResponse listArticleResponse) {
                dataSet = listArticleResponse;
                setUpRecyclerView();
                if (CURRENT_PAGE<=TOTAL_PAGE){
                    adapter.addBottomItem();
                }else{
                    isLastPage = true;
                }
                swContainer.setRefreshing(false);
                Log.d("Main","Result : "+dataSet);
            }
        });
    }

    // TODO : method setup Swipe Refresh when use scroll to top in recyclerView.
    private void setUpSwipeRefreshLayout(){
        swContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listArticleViewModel.getPagination().setPage(START_PAGE);
                getLiveDataFromViewModel();
                listArticleViewModel.FetchListArticle();
            }
        });
    }
    
    /*
    * method override of interface in recyclerView and pass data to DetailActivity class.
    * */
    @Override
    public void getItemByClick(Article article) {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("Article",article);
        startActivity(intent);
    }

    @Override
    public void getItemFormDelete(Article article) {
        DeleteArticle(article);
    }

    @Override
    public void getItemFormUpdate(Article article) {
        UpdateArticle(article);
    }

    final int MY_REQUEST_CODE = 42;

    // TODO: implement method update
    public void UpdateArticle(Article article){

        Intent intent = new Intent(MainActivity.this, UpdateActivity.class);
        intent.putExtra("article",article);
        startActivity(intent);
    }

    private CountDownTimer countDownTimer;
    private long millisTime;
    final boolean[] isDelete = {false};

    // TODO: implement method Delete
    public void DeleteArticle(Article article){
        startEvent(article);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Message is deleted", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbarOne = Snackbar.make(coordinatorLayout, "Message is restored!", Snackbar.LENGTH_SHORT);
                        isDelete[0] = true;
                        snackbarOne.show();
                    }
                });

        snackbar.show();
    }

    private void startEvent(final Article article) {

        countDownTimer = new CountDownTimer(3100,3100) {
            @Override
            public void onTick(long millisUntilFinished) {
                millisTime = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                Deleted(article);
            }
        }.start();
    }

    private void Deleted(Article article) {
        if (isDelete[0]){
            Toast.makeText(MainActivity.this,"Undo",Toast.LENGTH_LONG).show();
            isDelete[0] = false;
        }else{
            Log.d("Article",article.getTitle());
            listArticleViewModel.setId(article.getId());
            listArticleViewModel.initDeleteArticle();
            listArticleViewModel.getDeleteArticle().observe(this, new Observer<DeleteResponse>() {
                @Override
                public void onChanged(DeleteResponse deleteResponse) {
                    Log.d("delete","deleteArticle By ID : "+deleteResponse.getMessage());
                    listArticleViewModel.FetchListArticle();
                    getLiveDataFromViewModel();
                }
            });
            Toast.makeText(MainActivity.this, "delete", Toast.LENGTH_LONG).show();
        }
    }



}
