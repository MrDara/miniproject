package com.example.testams.view.update;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.testams.R;
import com.example.testams.model.api.entities.Article;
import com.example.testams.model.api.entities.PostArticle;
import com.example.testams.model.api.responses.ServerResponseImage;
import com.example.testams.model.api.responses.UpdateResponse;
import com.example.testams.view.add.AddActivity;
import com.example.testams.view.main.MainActivity;
import com.example.testams.viewmodel.ListArticleViewModel;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private ListArticleViewModel listArticleViewModel;

    protected EditText txtTitle,Description,Date;
    protected ImageView imageView;
    protected Button cancel,save;
    Article article = null;

    protected String TAG = "error";
    private final static int RESULT_LOAD_IMAGE = 1;
    protected ServerResponseImage dataSet = null;

    Uri uri;

    String filePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        Intent intent = getIntent();
        article = (Article) intent.getSerializableExtra("article");

        initializeLayout();
        connectViewModel();
        setInformationToLayout();
    }

    private void connectViewModel(){
        listArticleViewModel = ViewModelProviders.of(this).get(ListArticleViewModel.class);
        listArticleViewModel.init();
    }

    private void initializeLayout(){
        txtTitle = findViewById(R.id.update_edit_title);
        Description = findViewById(R.id.update_edit_description);
        Date = findViewById(R.id.update_edit_date);
        imageView = findViewById(R.id.update_image_view);
        save = findViewById(R.id.update_save_button);
        cancel = findViewById(R.id.update_cancel_button);

        save.setOnClickListener(this);
        cancel.setOnClickListener(this);
        imageView.setOnClickListener(this);

    }

    private void setInformationToLayout(){
        txtTitle.setText(article.getTitle());
        Description.setText(article.getDescription());
        Glide.with(this).load(article.getImageUrl()).placeholder(R.drawable.ramadan).into(imageView);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date date=null;
        try {
            date = formatter.parse(article.getCreatedDate());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        formatter = new SimpleDateFormat("dd MMM yyyy HH:mm");

        Date.setText(formatter.format(date));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.update_save_button:
                int id = article.getId();
                String title = txtTitle.getText().toString();
                String description = Description.getText().toString();
                if (title != null && !title.equals("") && !description.equals("") && description != null){

                    PostArticle postArticle = new PostArticle();
                    postArticle.setAuthorId(1);
                    postArticle.setCategoryId(1);
                    postArticle.setDescription(description);
                    postArticle.setTitle(title);

                    if (dataSet != null){
                        postArticle.setImage(dataSet.getData());
                        Glide.with(this).load(dataSet.getData()).into(imageView);
                    }else{
                        postArticle.setImage("http://110.74.194.124:15011/image-thumbnails/thumbnail-22f3907b-806b-4ab8-a4ff-55662890e4e3.jpg");
                        Glide.with(this).load(R.drawable.ramadan).into(imageView);
                    }

                    listArticleViewModel.setId(id);
                    listArticleViewModel.setPostArticle(postArticle);
                    listArticleViewModel.initUpdateArticle();
                    listArticleViewModel.getUpdateResponseMutableLiveData().observe(this, new Observer<UpdateResponse>() {
                        @Override
                        public void onChanged(UpdateResponse updateResponse) {
                            Log.d("Update","Update Success : " + updateResponse);
                            listArticleViewModel.FetchListArticle();
                        }
                    });
                    Intent intent = new Intent(UpdateActivity.this, MainActivity.class);
                    startActivity(intent);

                }else{
                    Toast.makeText(UpdateActivity.this,"Error",Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.update_cancel_button:
                finish();
                break;
            case R.id.update_image_view:
                uploadImage();

        }
    }


    public void uploadImage(){
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    Intent intentImage = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentImage,RESULT_LOAD_IMAGE);
                }catch (Exception e){
                    Log.e(TAG,"ERROR : " + e.getMessage());
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK){
            uri = data.getData();
            imageView.setImageURI(uri);

            File file = new File(getPath(uri));


            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"),file);

            MultipartBody.Part body = MultipartBody.Part.createFormData("file",file.getName(),requestBody);

            String descriptionString =  "hello";

            RequestBody description = RequestBody.create(MultipartBody.FORM,descriptionString);

            listArticleViewModel.setFile(file);
            listArticleViewModel.setFileBody(requestBody);
            listArticleViewModel.setImage(body);
            listArticleViewModel.initUploadImage();
            Log.d("Success","File : " + String.valueOf(file));
            listArticleViewModel.getUploadImage().observe(this, new Observer<ServerResponseImage>() {
                @Override
                public void onChanged(ServerResponseImage serverResponseImage) {
                    dataSet = serverResponseImage;
                    Log.d("Success", String.valueOf(dataSet));
                }
            });
            Log.d(TAG, String.valueOf(data));

        }
    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

}
