package com.example.testams.view.detail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.testams.R;
import com.example.testams.model.api.entities.Article;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailActivity extends AppCompatActivity {

    private TextView textTitle,txtDescription,txtDate;
    private ImageView imageView;

    private void init(){
        textTitle = findViewById(R.id.detail_title_view);
        txtDescription = findViewById(R.id.detail_description_view);
        txtDate = findViewById(R.id.detail_date_view);
        imageView = findViewById(R.id.detail_image_view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        init();

        Intent intent = getIntent();
        Article article = (Article) intent.getSerializableExtra("Article");

        textTitle.setText(article.getTitle());
        txtDescription.setText(article.getDescription());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date=null;
        try {
            date = formatter.parse(article.getCreatedDate());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        formatter = new SimpleDateFormat("dd MMM yyyy HH:mm");
        txtDate.setText(formatter.format(date));


        Glide.with(this).load(article.getImageUrl()).placeholder(R.drawable.ramadan)
                .into(imageView);

    }

}