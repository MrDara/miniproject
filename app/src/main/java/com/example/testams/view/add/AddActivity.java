package com.example.testams.view.add;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.testams.R;
import com.example.testams.model.api.entities.PostArticle;
import com.example.testams.model.api.responses.ServerResponseImage;
import com.example.testams.view.main.MainActivity;
import com.example.testams.viewmodel.ListArticleViewModel;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class AddActivity extends AppCompatActivity {

    private static final int STORAGE_PERMISSION_CODE = 123;
    protected ServerResponseImage dataSet = null;
    private ListArticleViewModel listArticleViewModel;

    private ImageView imageView;
    protected EditText title,description,editDate;
    protected Button save,cancel;

    private final static int RESULT_LOAD_IMAGE = 1;
    private String TAG = "error";

    Uri uri;

    String filePath = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        connectViewModel();
        init();

        requestStoragePermission();

    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void connectViewModel(){
        listArticleViewModel = ViewModelProviders.of(this).get(ListArticleViewModel.class);
        listArticleViewModel.init();
    }

    private void init(){
        imageView = findViewById(R.id.add_image_view);
        title = findViewById(R.id.add_edit_title);
        description = findViewById(R.id.add_edit_description);
        save = findViewById(R.id.add_save_button);
        cancel = findViewById(R.id.add_cancel_button);
        editDate = findViewById(R.id.add_edit_date);
        saveToServer();
        uploadImage();
        cancel();

        Date currentTime = Calendar.getInstance().getTime();
        editDate.setText(currentTime.toString());
        editDate.setEnabled(false);
    }

    public void saveToServer(){
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Title = getTextFormEditTitle();
                String Description = getTextFormEditDescription();
                if (Title != null && !Title.equals("") && !Description.equals("") && Description != null){

                    PostArticle postArticle = new PostArticle();
                    postArticle.setAuthorId(1);
                    postArticle.setCategoryId(1);
                    postArticle.setDescription(Description);
                    postArticle.setTitle(Title);
//                    postArticle.setImage("http://110.74.194.124:15011/image-thumbnails/thumbnail-22f3907b-806b-4ab8-a4ff-55662890e4e3.jpg");

                    if (dataSet != null){
                        postArticle.setImage(dataSet.getData());
                    }else{
                        postArticle.setImage("http://110.74.194.124:15011/image-thumbnails/thumbnail-347adcea-cbd6-4938-8135-6730fe6ff8ba.jpg");
                    }

                    getPostArticle(postArticle);
                    Intent intent = new Intent(AddActivity.this, MainActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(AddActivity.this,"Error",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public void uploadImage(){
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    Intent intentImage = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentImage,RESULT_LOAD_IMAGE);
                }catch (Exception e){
                    Log.e(TAG,"ERROR : " + e.getMessage());
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK){
            uri = data.getData();
            imageView.setImageURI(uri);

            File file = new File(getPath(uri));


            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"),file);

            MultipartBody.Part body = MultipartBody.Part.createFormData("file",file.getName(),requestBody);

            String descriptionString =  "hello";

            RequestBody description = RequestBody.create(MultipartBody.FORM,descriptionString);

            listArticleViewModel.setFile(file);
            listArticleViewModel.setFileBody(requestBody);
            listArticleViewModel.setImage(body);
            listArticleViewModel.initUploadImage();
            Log.d("Success","File : " + String.valueOf(file));
            listArticleViewModel.getUploadImage().observe(this, new Observer<ServerResponseImage>() {
                @Override
                public void onChanged(ServerResponseImage serverResponseImage) {
                    dataSet = serverResponseImage;
                    Log.d("Success", String.valueOf(dataSet));
                }
            });
            Log.d(TAG, String.valueOf(data));

        }

    }

    public void cancel(){
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public String getTextFormEditTitle(){
        if(title.getText().toString() == null){
            Toast.makeText(AddActivity.this,"Please Enter Title",Toast.LENGTH_LONG).show();
            return null;
        }else{
            return title.getText().toString();
        }
    }
    public String getTextFormEditDescription(){
        if(description.getText().toString() == null){
            Toast.makeText(AddActivity.this,"Please Enter Description",Toast.LENGTH_LONG).show();
            return null;
        }else{
            return description.getText().toString();
        }
    }


    public void getPostArticle(PostArticle postArticle){
        listArticleViewModel.setPostArticle(postArticle);
        listArticleViewModel.intiPostArticle();
        listArticleViewModel.getPostArticle().observe(this, new Observer<PostArticle>() {
            @Override
            public void onChanged(PostArticle postArticle) {

            }
        });
    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

}